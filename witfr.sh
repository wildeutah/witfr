#!/bin/bash
# witfr robot code

function witfr (){

	drprog="main.py"
	wkdir="~/Desktop/witfr/src"
	indir=`pwd`

	# move to src directory
	cd $wkdir

	# execute program
	echo "starting robot"
	python3 $drprog

	# stop motors in case of error
	python3 sCMD.py -md 0 -mt 0
	echo "robot stopped"

	# return to original directory
	cd $indir
}

witfr
