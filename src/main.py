from time import sleep
from hardware.DeviceFactory import make_AttitudeSensor, make_DistanceSensor, make_MotionSensor, make_MotorDriver
# from hardware.MotorDriver import MotorDriver
# from hardware.AttitudeSensor import AttitudeSensor
# from gpiozero import MotionSensor
# from gpiozero import DistanceSensor


# turn in a square without using sensors
def blind_square(motor_driver):
    while True:
        motor_driver.set_course(100, 0)
        print("forward")
        sleep(5)
        motor_driver.set_course(0, 200)
        print("turn right")
        sleep(2.86)


def controlled_turn(md, ats, angle, max_turn=500, err=5):
    has_turned = 0
    rate = 0
    ds = make_DistanceSensor()
    while abs(angle - has_turned) > err:
        md.set_course(200, 0)
        sleep(4)
        print("pitch: " + str(ats.get_pitch()))
        print("roll: " + str(ats.get_roll()))
        print(ds.distance)
        md.set_course(0, 200)
        sleep(2)


# driving code
if __name__ == "__main__":
    with make_MotorDriver() as md:
    	controlled_turn(md, make_AttitudeSensor(), 90)
