from bluedot import BlueDot
from src.hardware.MotorDriver import MotorDriver
from signal import pause


def speed(pos):
    max_speed = 300
    return pos * max_speed

def move(pos):
    md.set_drive(speed(pos.y))
    md.set_turn(speed(pos.x))

if __name__ == '__main__':
    bd = BlueDot()
    with MotorDriver() as md:

        bd.when_pressed = move
        bd.when_moved = move
        bd.when_released = md.stop

        pause()
