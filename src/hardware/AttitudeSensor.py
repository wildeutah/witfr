from hardware.config.ConfigParser import read_config
import smbus
import math
import time

class AttitudeSensor:
# store registers
    PWR_MGMT_1 = 0x6b
    PWR_MGMT_2 = 0x6c
    TEMP_OUT = 0x41
    GYRO_XOUT = 0x43
    GYRO_YOUT = 0x45
    GYRO_ZOUT = 0x47
    ACCEL_XOUT = 0x3b
    ACCEL_YOUT = 0x3d
    ACCEL_ZOUT = 0x3f
    GYRO_CONFIG = 0x1b
    ACCEL_CONFIG = 0x1c

# dunder methods
    def __init__(self, config_file='hardware/config/attitude_sensor.txt'):
        self.bus = smbus.SMBus(1)  # or bus = smbus.SMBus(1) for Revision 2 boards
        self.address = 0x68  # This is the address value read via the i2cdetect command
        self.__hardware_setup()
        self.config = read_config(config_file)

# static methods
    @staticmethod
    def DEFAULT(x):
        return x

    @staticmethod
    def DEGpSEC(x):
        return x / 131.0

    @staticmethod
    def Gs(x):
        return x / 16384.0

    @staticmethod
    def DEGC(x):
        return (x / 340.0) + 36.53

    @staticmethod
    def dist(a, b):
        return math.sqrt((a * a) + (b * b))

# private methods
    def __hardware_setup(self):
        # Now wake the 6050 up as it starts in sleep mode
        self.bus.write_byte_data(self.address, AttitudeSensor.PWR_MGMT_1, 0)

    def __read_byte(self, adr):
        return self.bus.read_byte_data(self.address, adr)

    def __read_word(self, adr):
        high = self.bus.read_byte_data(self.address, adr)
        low = self.bus.read_byte_data(self.address, adr + 1)
        val = (high << 8) + low
        return val

    def __read_word_2c(self, adr):
        val = self.__read_word(adr)
        if val >= 0x8000:
            return -((65535 - val) + 1)
        else:
            return val

# protected methods
    def _read_reg(self, reg, units=None):
        res = self.__read_word_2c(reg)
        if units is not None:
            return units(res)
        return res

# public methods
    # read temperature data and report in degrees celcius
    def temp(self, units=None):
        res = self.__read_word_2c(AttitudeSensor.TEMP_OUT)
        if units is not None:
            return units(res)
        else:
            return AttitudeSensor.DEGC(res)

    # read right axis gyro data and report in degrees per second
    def gyro_right(self, units=None):
        if self.config.get('right_axis')[0] == 'z':
            res = self.__read_word_2c(AttitudeSensor.GYRO_ZOUT)
        elif self.config.get('right_axis')[0] == 'y':
            res = self.__read_word_2c(AttitudeSensor.GYRO_YOUT)
        else:
            res = self.__read_word_2c(AttitudeSensor.GYRO_XOUT)

        res *= self.config.get('right_axis')[1]
        if units is not None:
            return units(res)
        else:
            return AttitudeSensor.DEGpSEC(res)

    # read forward axis gyro data and report in degrees per second
    def gyro_forward(self, units=None):
        if self.config.get('forward_axis')[0] == 'z':
            res = self.__read_word_2c(AttitudeSensor.GYRO_ZOUT)
        elif self.config.get('forward_axis')[0] == 'y':
            res = self.__read_word_2c(AttitudeSensor.GYRO_YOUT)
        else:
            res = self.__read_word_2c(AttitudeSensor.GYRO_XOUT)

        res *= self.config.get('forward_axis')[1]
        if units is not None:
            return units(res)
        else:
            return AttitudeSensor.DEGpSEC(res)

    # read vertical axis gyro data and report in degrees per second
    def gyro_vertical(self, units=None):
        if self.config.get('vertical_axis')[0] == 'z':
            res = self.__read_word_2c(AttitudeSensor.GYRO_ZOUT)
        elif self.config.get('vertical_axis')[0] == 'y':
            res = self.__read_word_2c(AttitudeSensor.GYRO_YOUT)
        else:
            res = self.__read_word_2c(AttitudeSensor.GYRO_XOUT)

        res *= self.config.get('vertical_axis')[1]
        if units is not None:
            return units(res)
        else:
            return AttitudeSensor.DEGpSEC(res)

    # read right axis accelerometer data and report in g's
    def accel_right(self, units=None):
        if self.config.get('right_axis')[0] == 'z':
            res = self.__read_word_2c(AttitudeSensor.ACCEL_ZOUT)
        elif self.config.get('right_axis')[0] == 'y':
            res = self.__read_word_2c(AttitudeSensor.ACCEL_YOUT)
        else:
            res = self.__read_word_2c(AttitudeSensor.ACCEL_XOUT)

        res *= self.config.get('right_axis')[1]
        if units is not None:
            return units(res)
        else:
            return AttitudeSensor.Gs(res)

    # read forward axis accelerometer data and report in g's
    def accel_forward(self, units=None):
        if self.config.get('forward_axis')[0] == 'z':
            res = self.__read_word_2c(AttitudeSensor.ACCEL_ZOUT)
        elif self.config.get('forward_axis')[0] == 'y':
            res = self.__read_word_2c(AttitudeSensor.ACCEL_YOUT)
        else:
            res = self.__read_word_2c(AttitudeSensor.ACCEL_XOUT)

        res *= self.config.get('forward_axis')[1]
        if units is not None:
            return units(res)
        else:
            return AttitudeSensor.Gs(res)

    # read vertical axis accelerometer data and report in g's
    def accel_vertical(self, units=None):
        if self.config.get('vertical_axis')[0] == 'z':
            res = self.__read_word_2c(AttitudeSensor.ACCEL_ZOUT)
        elif self.config.get('vertical_axis')[0] == 'y':
            res = self.__read_word_2c(AttitudeSensor.ACCEL_YOUT)
        else:
            res = self.__read_word_2c(AttitudeSensor.ACCEL_XOUT)

        res *= self.config.get('vertical_axis')[1]
        if units is not None:
            return units(res)
        else:
            return AttitudeSensor.Gs(res)

    # angle around y axis in degrees
    def get_roll(self):
        r = self.accel_right()
        f = self.accel_forward()
        v = self.accel_vertical()

        radians = math.atan2(r, AttitudeSensor.dist(f, v))
        return -math.degrees(radians)
    
    # angle around x axis in degrees
    def get_pitch(self):
        r = self.accel_right()
        f = self.accel_forward()
        v = self.accel_vertical()
        radians = math.atan2(f, AttitudeSensor.dist(r, v))
        return math.degrees(radians)

if __name__ =="__main__":
    at = AttitudeSensor()
    sleep_time = 0.1
    cur_ang = 0
    while True:
        print(at.get_pitch())
#        cur_ang += at.gyro_z() * sleep_time
#        print(cur_ang)
        time.sleep(sleep_time)
