def read_config(filename):
    with open(filename, 'r') as f:
        results = {}
        for line in f:
            s = line.strip()
            if s.startswith('#') or len(s) == 0:
                continue
            s = s.strip().split(': ')
            results[s[0]] = atom(s[1])
    return results


def atom(elem):
    elem = elem.strip()
    try:
        return int(elem)
    except ValueError:
        pass
    try:
        return float(elem)
    except ValueError:
        pass
    if elem == "TRUE":
        return True
    if elem == "FALSE":
        return False
    if ' ' in elem:
        elems = elem.split(' ')
        for i in range(len(elems)):
            elems[i] = atom(elems[i])
        return elems

    return elem

if __name__ == "__main__":
    ast = read_config('attitude_sensor.txt')
    print(ast)