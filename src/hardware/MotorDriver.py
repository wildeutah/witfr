# import webiopi
from webiopi.devices.serial import Serial
from hardware.config.ConfigParser import read_config


# class to drive sabertooth via serial commands
class MotorDriver:
    # all dunder methods
    def __init__(self, config_file='hardware/config/motor_driver.txt', port=None, baud_rate=None):
        if port is None and baud_rate is None:
            config = read_config(config_file)
            self.serial = Serial(config.get("serial_port"), config.get("baud_rate"))  # serial object to send data over usb
        else:
            self.serial = Serial(port, baud_rate)  # serial object to send data over usb

        self.__md = 0		# store drive value
        self.__mt = 0		# store turn value
        self.set_course(0, 0)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.stop()

    def __del__(self):
        self.stop()

    def __delete__(self, instance):
        self.stop()

    # set drive and turn
    def set_course(self, drive, turn):
        if drive is not None:
            self.set_drive(drive)
        if turn is not None:
            self.set_turn(turn)

    # return a tuple of current drive rate and turn rate
    def get_course(self):
        return self.__md, self.__mt

    # write drive value
    def set_drive(self, drive):
        if drive != self.__md:
            self.serial.writeString('MD: ' + str(drive) + "\r\n")
            self.__md = drive

    # return current drive value
    def get_drive(self):
        return self.__md

    # write turn value
    def set_turn(self, turn):
        if turn != self.__mt:
            self.serial.writeString('MT: ' + str(turn) + "\r\n")
            self.__mt = turn

    # get current turn value
    def get_turn(self):
        return self.__mt

    # stop motion
    def stop(self):
        self.set_course(0, 0)
        print("motors stopped")


if __name__ == "__main__":
    with MotorDriver() as md:
        pass
