from gpiozero import DistanceSensor
from hardware.config.ConfigParser import read_config


def make_DistanceSensor(config_file='hardware/config/distance_sensor.txt'):
    config = read_config(config_file)
    return DistanceSensor(config.get('echo_pin'), config.get('trigger_pin'))
