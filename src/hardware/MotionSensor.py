from gpiozero import MotionSensor
from hardware.config.ConfigParser import read_config


def make_MotionSensor(config_file='hardware/config/motion_sensor.txt'):
    config = read_config(config_file)
    return MotionSensor(config.get("input_pin"))


