from hardware.config.ConfigParser import read_config
from hardware.MotorDriver import MotorDriver
from hardware.AttitudeSensor import AttitudeSensor
from gpiozero import MotionSensor
from gpiozero import DistanceSensor


def make_MotorDriver(config_file='hardware/config/motor_driver.txt'):
    config = read_config(config_file)
    return MotorDriver(port=config.get("serial_port"), baud_rate=config.get("baud_rate"))


def make_AttitudeSensor(config_file='hardware/config/attitude_sensor.txt'):
    return AttitudeSensor(config_file)


def make_DistanceSensor(config_file='hardware/config/distance_sensor.txt'):
    config = read_config(config_file)
    return DistanceSensor(config.get('echo_pin'), config.get('trigger_pin'))


def make_MotionSensor(config_file='hardware/config/motion_sensor.txt'):
    config = read_config(config_file)
    return MotionSensor(config.get("input_pin"))
