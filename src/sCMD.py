import sys
import argparse
import webiopi
from webiopi.devices.serial import Serial

serial = Serial('/dev/ttyACM0', 9600)

parser = argparse.ArgumentParser()
parser.add_argument('-md', '--motor_drive', help="set motor drive speed", required=False)
parser.add_argument('-mt', '--motor_turn', help="set motor turn speed", required=False)
parser.add_argument('-g', '--get_values', help="return drive and turn values", required=False)

args = parser.parse_args()
if args.motor_drive is not None:
    serial.writeString("MD: " + args.motor_drive + "\r\n")
if args.motor_turn is not None:
    serial.writeString("MT: " + args.motor_turn + "\r\n")
if args.get_values is not None:
    print(serial.writeString("MD: get\r\n"))
    print(serial.writeString("MT: get\r\n"))

