import xbox
from src.hardware.MotorDriver import MotorDriver
import time

class xbox_control:
    def __init__(self, max_speed=200, max_turn=200):
        self.md = MotorDriver()
        self.joy = xbox.Joystick()
        self.max_speed = max_speed
        self.max_turn = self.max_turn
        self.speed_cap = 800
        self.turn_cap = 800
        if not self.joy.conected():
            raise Exception("Controller Not Connected")

    def __enter__(self):
        return self

    def __exit__(self):
        self.md.stop()
        self.joy.close()

    def stop_wait(self):
        self.md.stop()
        while self.joy.connected() and not self.joy.back():
            if self.joy.B():
                time.sleep(0.1)
                if not self.joy.B():
                    return
            else:
                time.sleep(0.1)


    def dance(self):
        self.md.stop()
        for i in range(3):
            self.md.set_turn(250)
            time.sleep(0.25)
            self.md.set_turn(0)
            time.sleep(0.15)
            self.md.set_turn(-250)
            time.sleep(0.25)
            self.md.set_turn(0)
            time.sleep(0.15)

        for j in range(2):
            self.md.set_turn(-150)
            time.sleep(0.5)
            self.md.set_turn(0)
            time.sleep(0.15)
            self.md.set_turn(150)
            time.sleep(0.5)
            self.md.set_turn(0)
            time.sleep(0.15)


    def loop(self):
        while self.joy.connected() and not self.joy.back():

            # use b button to temporarily freeze robot
            if self.joy.B():
                self.stop_wait()

            # use y button to make the robot dance
            if self.joy.Y():
                self.dance()

            # control motors with left stick
            t, d = self.joy.leftStick()
            self.md.set_course(int(self.max_speed*d), int(self.max_turn*t))

            # shift max speed up and down by 50
            if self.joy.dpadDown() and self.max_speed > 0:
                time.sleep(.1)
                if not self.joy.dpadDown():
                    self.max_speed -= 50

            if self.joy.dpadUp() and self.max_speed < self.speed_cap:
                time.sleep(.1)
                if not self.joy.dpadUp():
                    self.max_speed += 50

            # shift max turn up and down by 50
            if self.joy.dpadLeft() and self.max_turn > 0:
                time.sleep(.1)
                if not self.joy.dpadLeft():
                    self.max_turn -= 50

            if self.joy.dpadRight() and self.max_turn < self.turn_cap:
                time.sleep(.1)
                if not self.joy.dpadRight():
                    self.max_turn += 50


if __name__ == "__main__":
    with xbox_control() as xb:
        xb.loop()
