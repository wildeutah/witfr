from unused_code.MPU6050 import MPU6050
from hardware.config.ConfigParser import read_config


class AttitudeSensor(MPU6050):
    def __init__(self, config_file='config/attitude_sensor.txt', vertical_axis=None):
        super().__init__()
        super().dmp_initialize()
        if vertical_axis is None:
            config = read_config(config_file)
            self.vertical_axis = config.get("vertical_axis")  # This must be kept up to date with how the gyro is mounted

        else:
            self.vertical_gyro_axis = vertical_axis  # This must be kept up to date with how the gyro is mounted



    # get current acceleration data in terms of gravity
    def get_acceleration(self, raw_units=False):
        data = self.get_acceleration()
        if raw_units:
            return data
        data = [i / 16384.0 for i in data]  # convert to terms of gravity
        return tuple(data)

    # get current gyroscope data in degrees per second
    def get_rotation(self, raw_units=False):
        data = self.get_rotation()
        if raw_units:
            return data
        data = [i / 131.0 for i in data]  # convert to degrees per second
        return tuple(data)

    def get_turn(self, raw_units=False):
        data = self.get_rotation()[self.vertical_axis]
        if raw_units:
            return data
        return data / 131.0

if __name__ == "__main__":
    ats = AttitudeSensor(1)
