from abc import ABC
import RPi.GPIO as GPIO


class UniqueDevice(ABC):
    def __init__(self, outPins, inPins):
        self.outPins = outPins
        self.inPins = inPins

        GPIO.setmode(GPIO.BOARD)
        # setup all output pins
        for op in self.outPins:
            GPIO.setup(op, GPIO.OUT)
        # setup all input pins
        for ip in self.inPins:
            GPIO.setup(ip, GPIO.IN)

    def destroy(self):
        for op in self.outPins:
            GPIO.output(op, GPIO.LOW)  # set output pins to low

