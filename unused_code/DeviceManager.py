from hardware.MotorDriver import MotorDriver
from hardware.AttitudeSensor import AttitudeSensor
from hardware.DistanceSensor import DistanceSensor
from hardware.MotionSensor import MotionSensor
from hardware.config.ConfigParser import read_config
from time import sleep


# wrapper class so that all hardware objects can be created and cleaned up using with statement
class DeviceManager:
    # all dunder methods
    def __init__(self, config_file='hardware/config/device_manager.txt', config_dict=None):
        if config_dict is None:
            self.config = read_config(config_file)
        else:
            self.config = config_dict

        if self.config.get("motor_driver"):
            self.motor_driver = MotorDriver()  # motor driver object

        if self.config.get("attitude_sensor"):
            self.attitude_sensor = AttitudeSensor()  # attitude sensor object

        if self.config.get("motion_sensor"):
            self.motion_sensor = MotionSensor()  # motion sensor object

        if self.config.get("distance_sensor"):
            self.distance_sensor = DistanceSensor()  # distance sensor

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        if self.config.get("motor_driver"):
            self.motor_driver.stop()
            del self.motor_driver
        if self.config.get("attitude_sensor"):
            del self.attitude_sensor
        if self.config.get("distance_sensor"):
            del self.distance_sensor
        if self.config.get("motion_sensor"):
            del self.motion_sensor
        print("destroyed")

# all object getter methods
    # get motor driver object
#    def motor_driver(self):
#        if config.get("motor_driver"):
#            return self.motor_driver
#        else:
#            return None
        
    # get attitude sensor objet
#    def attitude_sensor(self):
#        if config.get("attitude_sensor"):
#            return self.attitude_sensor
#        else:
#            return None


 #   def motion_sensor(self):
 #       if config.get("motion_sensor"):
 #           return self.motion_sensor
 #       else:
 #           return None
        
 #   def distance_sensor(self):
 #       if config.get("distance_sensor"):
 #           return self.distance_sensor
 #       else:
 #           return None
        
# all working methods
    # set robot drive and turn
#    def set_course(self, drive=None, turn=None):
#        self.motor_driver.set_course(drive, turn)

    # #linear aproximation of sigmoid for turn rate
    # def calc_turn_rate_slow(self, angle, max_turn_rate, deg_error=3):
    #     sign = 1
    #     if angle < 0:
    #         sign *= -1
    #         angle = abs(angle)
    #     if angle < deg_error:
    #         return 0
    #     if angle < 45:
    #         return

 #   def calc_turn_rate_lin(self, angle, max_turn_rate, deg_error=3):
 #       sign = 1
 #       if angle < 0:
 #           sign *= -1
 #           angle = abs(angle)
 #       if angle <deg_error:
 #           return 0
 #       if angle > 90:
 #           return max_turn_rate * sign
 #       else:
 #           return ((max_turn_rate-100)*(angle-deg_error)/(90-deg_error))+100

#    def calc_turn_rate_full(self, angle, max_turn_rate, deg_error=3):
#        if abs(angle) > deg_error:
#            return 0
#        if angle > 0:
#            return max_turn_rate
#        else: return -max_turn_rate


    # set damping turn rate based on desired direction
#    def smart_turn(self, angle, turn_rate):
#        wait_time = 0.0001
#        turned_angle = 0
#        self.motor_driver.set_turn(turn_rate)
#        while abs(turned_angle) > angle:
#            tr = self.calc_turn_rate_lin(turned_angle, turn_rate)
#            if tr == 0:
#                self.motor_driver.set_turn(0)
#                return
#            self.motor_driver.set_turn(tr)
#            sleep(wait_time)
            # wait for acceleration before taking turn rate measurement
#            turned_angle += self.attitude_sensor.get_turn() * wait_time
#        self.motor_driver.set_turn(0)

    # get turn data from gyroscope, vertical axis only
#    def get_turn(self, raw_units=False):
#        return self.attitude_sensor.get_turn(raw_units)

    # check motion sensor for detected motion
#    def check_motion(self):
#        return self.motion_sensor.check_motion()

#    def get_distance(self):
#        return self.distance_sensor.get_distance()


