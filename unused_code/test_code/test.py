from abc import ABC, abstractmethod
#import numpy as np
from unused_code.test_code.Help import Help

class test:
    def __init__(self):
        print("init")

    def __enter__(self):
        print("enter")

    def __exit__(self, exc_type, exc_value, traceback):
        print("exit")

    def testfun(self):
        self.abc=4

    def testthetest(self):
        print(self.abc)

def make_test():
    return test()

class a(ABC):
    @abstractmethod
    def testFun(self):
        pass

class b(a):
    def __init__(self):
        self.x = 1

    def testFun(self):
        return self.x

    def testInherit(self):
        print("inheritance works")

class c(b):
    def __init__(self):
        super().__init__()


if __name__ == '__main__':
    with make_test() as t:
        print("working")

