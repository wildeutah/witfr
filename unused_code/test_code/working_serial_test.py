#!/usr/bin/python3
#sabertooth usb device at "dev/ttyACM0"

import webiopi
from webiopi.devices.serial import Serial
from time import sleep

class Witfr:
    def __init__(self):
        self.serial = Serial('/dev/ttyACM0', 9600) #enter correct port for serial
        self.md = 0
        self.serial.writeString("MD: " + str(drive) + "\r\n")
        self.mt = 0
        self.serial.writeString("MT: " + str(drive) + "\r\n")
        
    def set_heading(self, drive=self.md, turn=self.mt):
        self.md = drive
        self.serial.writeString("MD: " + str(drive) + "\r\n")
        self.mt = turn
        self.serial.writeString("MT: " + str(turn) + "\r\n")

    def stop(self):
        self.set_heading(0, 0)
        print("stoped")
    
    def loop(self):
        self.set_heading(500, 0)
        print("forward")
        sleep(2)
        self.set_heading(0, 500)
        print("turn right")
        sleep(0.5)
        self.set_heading(-500, 0)
        print("backward")
        sleep(2)
        self.set_heading(0, -500)
        print("turn left")
        sleep(0.5)
        
    
#def loop_old():
#    # write data to serial port
#    for i in range (1, 5):
#        serial.writeString("M1: 1023\r\n")
#        print("M1 Fwd")
#        sleep(2)
#        serial.writeString("M1: 0\r\n")
#        print("M1 Off")
#        sleep(2)
#        serial.writeString("M2: 1023\r\n")
#        print("M2 Fwd")
#        sleep(2)
#        serial.writeString("M2: 0\r\n")
#        print("M2 Off")
#        sleep(2)
#        serial.writeString("M1: -1023\r\n")
#        print("M1 Reverse")
#        sleep(2)
#        serial.writeString("M1: 0\r\n")
#        print("M1 Off")
#        sleep(2)
#        serial.writeString("M2: -1023\r\n")
#        print("M2 Reverse")
#        sleep(2)
#        serial.writeString("M2: 0\r\n")
#        print("M2 Off")
#
#        if (serial.available() > 0):
#            data = serial.readString()
#            print(data)
#
#        webiopi.sleep(1)
        
if __name__ == "__main__":
    witfr = Witfr()
    try:
        witfr.loop()
    except KeyboardInterrupt:
        witfr.stop()
        
