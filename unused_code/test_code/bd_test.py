from bluedot import BlueDot
from signal import pause

def speed(d):
    max_speed = 300
    return int(d * max_speed)

def press(pos):
    if pos.top:
        print("top")
    elif pos.bottom:
        print("bottom")
    elif pos.left:
        print("left")
    elif pos.right:
        print("right")
    else:
        print("center")

def move(pos):
    if pos.top:
        print("top")
        print(speed(pos.distance))
    elif pos.bottom:
        print("bottom")
        print(speed(pos.distance))
    elif pos.left:
        print("left")
        print(speed(pos.distance))
    elif pos.right:
        print("right")
        print(speed(pos.distance))
    else:
        print("center")
        
def move_smooth(pos):
    print("X: " + str(speed(pos.x)))
    print("Y: " + str(speed(pos.y)))
    
        
def stop(pos):
    print("stop")
    
def rotated(rot):
    print("rotated")
    
def swiped(swp):
    print("swiped")
    
def double_pressed(pos):
    print("double pressed")

bd = BlueDot()

#bd.when_pressed = press
bd.when_moved = move_smooth
bd.when_released = stop
bd.when_rotated = rotated
bd.when_swiped = swiped
bd.when_double_pressed = double_pressed

pause()
