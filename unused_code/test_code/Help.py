import numpy as np


class Help:
    @staticmethod
    def make_array():
        return np.zeros((2,2), dtype=int)
