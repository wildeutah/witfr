import RPi.GPIO as GPIO
from unused_code.UniqueDevice import UniqueDevice
import time
from math import inf


class UltrasonicSensor(UniqueDevice):
    def __init__(self, config):
        # store pin numbers
        self.trigger_pin = config.get("trigger_pin")
        self.echo_pin = config.get("echo_pin")

        # initialize parent class
        super().__init__(outPins=[self.trigger_pin], inPins=[self.echo_pin])

        self.max_distance = config.get("max_distance")  # define the maximum measured distance(cm)
        self.time_out = self.max_distance * 60  # calculate timeout(μs) according to the maximum measured

    # get the measurement results of ultrasonic module, with unit: cm
    def get_distance(self):
        # send sonar pulse
        GPIO.output(self.trigger_pin, GPIO.HIGH) # make trigPin send 10us high level
        time.sleep(0.00001) # 10us
        GPIO.output(self.trigger_pin, GPIO.LOW)

        # time response
        t0 = time.time()
        while GPIO.input(self.echo_pin) != GPIO.HIGH:
            if (time.time() - t0) > self.time_out * 0.000001:
                return inf
        t0 = time.time()
        while GPIO.input(self.echo_pin) == GPIO.HIGH:
            if (time.time() - t0) > self.time_out * 0.000001:
                return inf
        pingTime = (time.time() - t0) * 0.000001

        # calculate distance
        return pingTime * 340.0 / 2.0 / 10000.0 # the sound speed is 340m/s, and calculate distance (cm) return distance

    def destroy(self):
        super().destroy()

